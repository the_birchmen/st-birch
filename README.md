st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.


Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install

Patches I have added
--------------------
- st-alpha-0.8.2.diff
- st-scrollback-0.8.2.diff
- st-scrollback-mouse-0.8.2.diff
- st-desktopentry-0.8.2.diff
- st-invert-0.8.2.diff
- st-xresources-20200604-9ba7ecf.diff
- st-newterm-0.8.2.diff
- st-bold-is-not-bright-20190127-3be4cf1.diff
